package com.example;

import com.example.logging.Log;
import com.example.worker.IConcatenator;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

public class Benchmarker {

    // using custom guice injection
    @Log private Logger logger;
    private final List<IConcatenator> concatenators = new ArrayList<>();

    /**
     * Adds concatenator to benchmark
     * @param concat
     */
    public void addConcatenator(IConcatenator concat) {
        // Using guava precondition
        concatenators.add(checkNotNull(concat));
    }

    /**
     * runs benchmark
     * @throws InterruptedException
     */
    public void run() throws InterruptedException {
        Random random = new Random();
        checkState(concatenators.size() > 0);

        for (IConcatenator concat: concatenators) {
            // Using jodatime
            DateTime startTime = DateTime.now();
            for (int j = 0; j < 100000; j++) {
                for (int i = 0; i < 10; i++) {
                    int next = random.nextInt(1000);
                    logger.debug("{}", next);
                    concat.addInt(next);
                }
                logger.debug(concat.getConcat());
            }
            logger.info(new Duration(DateTime.now(), startTime));
        }
    }

}
