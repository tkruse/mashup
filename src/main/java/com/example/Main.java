package com.example;

import org.apache.logging.log4j.Logger;

import com.example.logging.Log;
import com.example.logging.Log4JTypeListener;
import com.example.worker.ArrayConcatenator;
import com.example.worker.ArrayListConcatenator;
import com.example.worker.StringBuilderConcatenator;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.matcher.Matchers;

/**
 * Main CLI application launching the Benchmark
 */
public class Main extends AbstractModule {

    @Log
    Logger logger;

    @Override
    protected void configure() {
        // guice log injection
        bindListener(Matchers.any(), new Log4JTypeListener());
    }

    /**
     * start benchmark
     *
     * @param bench
     * @throws InterruptedException
     */
    public void run(Benchmarker bench) throws InterruptedException {
        bench.addConcatenator(new StringBuilderConcatenator());
        bench.addConcatenator(new ArrayListConcatenator());
        bench.addConcatenator(new ArrayConcatenator());
        bench.run();
    }

    /**
     * CLI main
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        // Manual Injecting with Guice
        Injector injector = Guice.createInjector(new Main());
        Main main = injector.getInstance(Main.class);
        Benchmarker bench = injector.getInstance(Benchmarker.class);
        main.run(bench);
    }
}
