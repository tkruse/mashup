package com.example.worker;

import com.google.common.primitives.Ints;

import java.util.Arrays;

public class ArrayConcatenator implements IConcatenator {

    private final int[] arr = new int[10];
    private int index = 0;

    @Override
    public void addInt(int value) {
        arr[index] = value;
        index++;
    }

    @Override
    public String getConcat() {
        try {
            return Ints.join("", Arrays.copyOfRange(arr, 0, index));
        } finally {
            index = 0;
        }
    }

}
