package com.example.worker;

public class ArrayListConcatenator implements IConcatenator {

    private StringBuilder builder = new StringBuilder();

    @Override
    public void addInt(int value) {
        builder.append(value);
    }

    @Override
    public String getConcat() {
        try {
            return builder.toString();
        } finally {
            builder.setLength(0);
        }
    }

}
