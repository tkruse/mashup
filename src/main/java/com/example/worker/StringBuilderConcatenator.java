package com.example.worker;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;

public class StringBuilderConcatenator implements IConcatenator {

    private ArrayList<Integer> alist = new ArrayList<>();

    @Override
    public void addInt(int value) {
        alist.add(value);
    }

    @Override
    public String getConcat() {
        try {
            return StringUtils.join(alist, ',');
        } finally {
            alist.clear();
        }
    }

}
