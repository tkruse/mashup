package com.example.worker;

/**
 * Simple example class
 */
public interface IConcatenator {

    /**
     * adds an int to the concantenation result
     * @param value
     */
    void addInt(int value);

    /**
     * @return a String concatenating all added Objects since last getConcat()
     */
    String getConcat();

}
