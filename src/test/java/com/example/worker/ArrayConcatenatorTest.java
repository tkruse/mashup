package com.example.worker;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class ArrayConcatenatorTest {

    @Test
    public void testAddInt() {
        new ArrayConcatenator().addInt(0);
    }

    @Test
    public void testGetConcat() throws Exception {
        // hamcrest matcher
        assertThat(new ArrayConcatenator().getConcat(), equalTo(""));

    }

}
