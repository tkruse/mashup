package com.example

import static org.mockito.Mockito.*

import org.apache.logging.log4j.Logger
import org.powermock.reflect.Whitebox

import spock.lang.*

import com.example.worker.IConcatenator

public class SpockBenchmarkerTest extends Specification {

	def nullInput() {

		when:
		new Benchmarker().addConcatenator(null)

		then:
		thrown(NullPointerException)
	}

	def noConcatenator() {

		when:
		new Benchmarker().run()

		then:
		thrown(IllegalStateException)
	}

        //@Ignore
        @Timeout(5)
	def singleConcatenator() {
		setup:
		IConcatenator concat = Mock(IConcatenator.class)
		Benchmarker bench = new Benchmarker()
		def logger = Stub(Logger.class)
		// Using Powermock reflection
		Whitebox.setInternalState(bench, "logger", logger)
		bench.addConcatenator(concat)
		when:
		bench.run()

		then:
		_ * concat.addInt(_)
	}
}
