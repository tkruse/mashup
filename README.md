JavaLibMashup
=============

This is just a sandbox project for myself to try out combining different popular java libraries / tools together.

I copy recipes from various sources into here.

Currently using:

- Gradle (plus diverse plugins)
- Log4j2
- Guava
- Guice
- commons-lang
- commons-collection
- joda-time
- JUnit
- Spock
- Mockito
- Hamcrest

Look at build.gradle for more details. Usage of those is not always reasonable.
